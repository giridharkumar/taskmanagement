const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;
const TaskSchema = new Schema( {
  "taskName": {
    type: String,
    trim: true,
    required:true
  },
  "taskEndDate": {
    type: Date,
    required: true
  },
  "description": {
    type: String,
    trim: true
  },
  "creationTimestamp": {
    type: Date,
    trim: true,
    default:new Date()
  },
  "createdBy": {
    type: String,
    trim: true,
    required:true
  }
} );
module.exports = mongoose.model('task',TaskSchema);
