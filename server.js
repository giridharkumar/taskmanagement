const cool = require( 'cool-ascii-faces' );
const express = require( 'express' );
let app = express();
const mongoose = require( 'mongoose' );
/* DeprecationWarning: Mongoose: mpromise (mongoose's default promise library) is deprecated, plug in your own promise library
 */
mongoose.Promise = global.Promise;
mongoose.connect( 'mongodb://localhost/taskmanagement' );
const bodyParser = require( 'body-parser' );
let logger = require( './utils/logger.js' );
app.use( express.static( __dirname + '/public' ) );
app.use( bodyParser.json() );
require( './apis/apis' )( app );
app.get( '/cool', function( request, response ) {
  response.send( cool() );
} );
app.listen( 3000, function( err ) {
  if ( err ) {
    logger.error( 'error while listening on port: %', 3000, err );
  } else {
    logger.info( 'server working fine on port: ', 3000 );
  }
} );
