let config = {
  development: {
    'port': 3001,
    'db': 'mongodb://localhost/taskmanagement'
  }
};
module.exports = config;
