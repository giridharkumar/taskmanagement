const mongoose = require( 'mongoose' );
const Task = require( '../models/Task' );
const logger = require( '../utils/logger.js' )
const restResponse = require( '../utils/restresponse.js' );

function TaskController() {
  this.createTask = function( req, res, next ) {
    console.log( 'createTask', req.body );
    let taskCreationReq = req.body;
    let task = new Task();
    task.taskName = taskCreationReq.name;
    task.taskEndDate = taskCreationReq.endDate;
    task.description = taskCreationReq.description;
    task.createdBy = taskCreationReq.createdBy;
    task.save( task, function( err, savedTask ) {
      if ( err ) {
        logger.error( 'error while creating task: ', err );
        restResponse.sendErrorResponse( res, 500, err );
      } else {
        restResponse.sendJsonSuccessResponse( res, savedTask );
      }
    } );
  }
  this.getTask = function( req, res ) {
    if ( req.params.taskId ) {
      Task.findOne( {
        _id: req.params.taskId
      }, function( err, savedTask ) {
        if ( err ) {
          logger.error( 'error while getting task: ', err );
          restResponse.sendErrorResponse( res, 500, err );
        } else {
          restResponse.sendJsonSuccessResponse( res, savedTask );
        }
      } );
    }
  }
  this.deleteTask = function( req, res ) {
    console.log( 'deleteTask' );
    Task.findOneAndRemove( {
      _id: req.params.taskId
    }, function( err, savedTask ) {
      if ( err ) {
        logger.error( 'error while deleting task: ', err );
        restResponse.sendErrorResponse( res, 500, err );
      } else {
        restResponse.sendJsonSuccessResponse( res, savedTask );
      }
    } )
  }
  this.updateTask = function( req, res ) {
    let updateReq = req.body;
    Task.findOneAndUpdate( {
      _id: req.params.taskId
    }, {
      $set: {
        taskName: updateReq.taskName,
        description: updateReq.description,
        taskEndDate: updateReq.taskEndDate,
        createdBy: updateReq.createdBy
      }
    }, function( err, updatedTask ) {
      if ( err ) {
        logger.error( 'error while updating task: ', err );
        restResponse.sendErrorResponse( res, 500, err );
      } else {
        restResponse.sendJsonSuccessResponse( res, updatedTask );
      }
    } );
  }
  this.getAllTask = function( req, res ) {
    console.log( 'get all task' );
    Task.find( {}, function( err, tasks ) {
      if ( err ) {
        logger.error( 'error while getting all task: ', err );
        restResponse.sendErrorResponse( res, 500, err );
      } else {
        restResponse.sendJsonSuccessResponse( res, tasks );
      }
    } );
  }
  this.filterTaskBeforEndDate = function( req, res ) {
    let endDate = req.params.date;
    Task.find( {
      taskEndDate: {
        $lte: endDate
      }
    }, function( err, tasks ) {
      if ( err ) {
        logger.error( 'error while filtering task by date: ', err );
        restResponse.sendErrorResponse( res, 500, err );
      } else {
        restResponse.sendJsonSuccessResponse( res, tasks );
      }
    } );
  }
  this.filterTaskAfterCreatedDate = function( req, res ) {
    let date = req.params.date;
    Task.find( {
      creationTimestamp: {
        $gte: date
      }
    }, function( err, tasks ) {
      if ( err ) {
        logger.error( 'error while filtering task by created date: ', err );
        restResponse.sendErrorResponse( res, 500, err );
      } else {
        restResponse.sendJsonSuccessResponse( res, tasks );
      }
    } );
  }
  this.filterTaskBetweenDateRange = function( req, res ) {
    let queryParams = req.query;
    Task.find( {
      creationTimestamp: {
        $gte: queryParams.startDate,
        $lte: queryParams.endDate
      }
    }, function( err, tasks ) {
      if ( err ) {
        logger.error( 'error while filtering task by date range: ', err );
        restResponse.sendErrorResponse( res, 500, err );
      } else {
        restResponse.sendJsonSuccessResponse( res, tasks );
      }
    } );
  }
} //closing TaskController
module.exports.TaskController = TaskController;
