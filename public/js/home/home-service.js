angular.module('app').factory('HomeService',function(Constants,$http){
	return{
		filterTaskByDate: function(fromDate,toDate){
			return $http.get(Constants.APP_URL+'filter-task-between-date?startDate='+fromDate+'&endDate='+toDate);

		},
		getAllTask: function(){
			return $http.get(Constants.APP_URL+'all-task');
		},
		deleteTask: function(taskId){
			return $http.delete(Constants.APP_URL+'task/'+taskId);
		},
		addTask: function(task){
			return $http.post(Constants.APP_URL+'task/',task);
		}
	}
});