angular.module( 'home', [] ).config( function( $stateProvider ) {
  $stateProvider.state( 'home', {
    url: '/home',
    templateUrl: 'js/home/home.tpl',
    controller: 'HomeCtrl'
  } );
} ).controller( 'HomeCtrl', function( $scope, HomeService, $mdDialog ) {
  console.log( 'HomeCtrl' );
  $scope.homeCtrl = {
    task: {}
  };
  $scope.customFullscreen = false;
  getAllTask();

  function getAllTask() {
    HomeService.getAllTask().success( function( res ) {
      $scope.homeCtrl.tasks = res.data;
    } );
  }
  /*filter task by date range*/
  $scope.filterTaskByDate = function( fromDate, toDate ) {
    if ( fromDate && toDate ) {
      let startDateInTimeStamp = Date.parse( fromDate );
      let endDateInTimeStamp = Date.parse( toDate );
      HomeService.filterTaskByDate( Date.parse( fromDate ), Date.parse( toDate ) ).success( function( res ) {
        $scope.homeCtrl.tasks = res.data;
        toastr.success( 'Task filtered successfully!' );
      } );
    }
  };
  $scope.deleteTask = function( taskId ) {
    if ( taskId ) {
      HomeService.deleteTask( taskId ).success( function( res ) {
        getAllTask();
        toastr.success( 'Task deleted successfully!' );
      } );
    }
  };
  //open md-dialog
  $scope.createTaskDialog = function( ev ) {
    $mdDialog.show( {
      controller: DialogController,
      templateUrl: 'js/home/add-task-dialog.html',
      // parent: angular.element( document.body ),
      targetEvent: ev,
      clickOutsideToClose: true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    } );
  };

  function DialogController( $scope, $mdDialog ) {
    $scope.hide = function() {
      $mdDialog.hide();
    };
    $scope.cancel = function() {
      $mdDialog.cancel();
    };
    $scope.answer = function( answer ) {
      $mdDialog.hide( answer );
    };
    $scope.addTask = function( task ) {
      if ( task ) {
        HomeService.addTask( task ).success( function( res ) {
          getAllTask();
          $mdDialog.hide( );
          toastr.success( 'Task added successfully!' );
          $scope.homeCtrl.task = {};
        } );
      }
    };
  }
} );
