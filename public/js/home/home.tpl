<div>
  <div class="well col-sm-12">
    <span>Task Management</span>
    <div style="float: right;">
      <label>From:</label>
      <md-datepicker ng-model="fromDate" md-placeholder="Enter date"></md-datepicker>
      <label>To:</label>
      <md-datepicker ng-model="toDate" md-placeholder="Enter date"></md-datepicker>
      <button ng-click="filterTaskByDate(fromDate,toDate)" ng-disabled="!fromDate && !toDate">Apply</button>
    </div>
  </div>
  <div nf-if="homeCtrl.tasks">
    <div class="well well-sm  col-sm-3" ng-repeat="task in homeCtrl.tasks">
      <label>{{task.taskName}}</label>
      <button class="" style="float: right;" title="Delete Task" ng-click="deleteTask(task._id)">X</button>
      <br>
      <label>{{task.taskEndDate | date}}</label>
      <br>
      <label>{{task.description}}</label>
    </div>
  </div>
  <div>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn well well-sm " style="width: 25%; height: 95px;" ng-click="createTaskDialog($event)">Create Task</button>
  </div>
</div>
