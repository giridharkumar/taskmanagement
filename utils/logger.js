const winston = require('winston');
let logger = new(winston.Logger)({
	transports:[
	new (winston.transports.Console)(),
	new (winston.transports.File)({filename:'./taskmanagementserver.log'})
	]
});

module.exports = logger;
