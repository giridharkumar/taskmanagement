const fs = require( 'fs' );
module.exports = function( app ) {
  let controllers = setupControllers();
  console.log( 'controllers', controllers );
  let taskController = new controllers.task.TaskController;
  //tasks apis
  app.post( '/task', taskController.createTask );
  app.get('/task/:taskId', taskController.getTask );
  app.delete( '/task/:taskId',taskController.deleteTask);
  app.put('/task/:taskId',taskController.updateTask);
  app.get( '/all-task', taskController.getAllTask);
  app.get( '/filter-task-before-enddate/:date',taskController.filterTaskBeforEndDate);
  app.get( '/filter-task-after-created-date/:date', taskController.filterTaskAfterCreatedDate);
  app.get( '/filter-task-between-date', taskController.filterTaskBetweenDateRange);
}

function setupControllers() {
  let controllers = {};
  let controllersPath = process.cwd() + '/controllers';
  fs.readdirSync( controllersPath ).forEach( function( file ) {
    if ( file.indexOf( '.js' ) != -1 ) {
      controllers[ file.split( '.' )[ 0 ] ] = require( controllersPath + '/' + file );
    }
  } );
  return controllers;
}
