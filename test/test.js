'use strict';
const chai = require('chai');
const expect = chai.expect;
chai.should();
function addNumberToItself(number){
	return number+number;
}
function multiplyTwoNumber(num1,num2){
	return num1 * num2;
}

function returnName(name){
 return name;
}
//describe is packaged version of test cases
describe('mathematical api test case',function(){
	it('returns the number added to itself',function(){
		addNumberToItself(5).should.equal(10);
	});
	it('returns the multiplication of two number',function(){
		multiplyTwoNumber(5,5).should.equal(25);
	});
});//closing describe
describe('general purpose api',function(){
	it('return name',function(){
		returnName('Giridhar').should.equal('Giridhar');
	});
});
